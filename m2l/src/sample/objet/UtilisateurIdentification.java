package sample.objet;

/**
 * Objet de type UtilisateurIdentification
 * Permet de tester si l'utilisateur existe
 */
public class UtilisateurIdentification {
    private String identifiant, mdp;

    /**
     * Constructeur UtilisateurIdentifiant
     * @param identifiant
     * @param mdp
     */
    public UtilisateurIdentification(String identifiant, String mdp){
        this.identifiant = identifiant;
        this.mdp = mdp;
    }

    /**
     * Récupère l'identifiant de l'UtilisateurIdentification.
     * @return
     */
    public String getIdentifiant() {
        return identifiant;
    }

    /**
     * Récupère le mot de passe de l'UtilisateurIdentification.
     * @return
     */
    public String getMdp() {
        return mdp;
    }

    /**
     * Réécrit la méthode toString de l'objet Salle
     * @return
     */
    @Override
    public String toString() {
        return "\t\tUtilisateurIdentification{" +
                "identifiant='" + identifiant + '\'' +
                ", mdp='" + mdp + '\'' +
                '}';
    }
}
