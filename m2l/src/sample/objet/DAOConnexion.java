package sample.objet;

import javafx.stage.Stage;
import sample.Main;
import sample.controller.CalendrierChoix;
import sample.controller.Menu;
import sample.controller.ReservationController;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * DAOConnexion permet de faire le lien entre l'application de la base de données.
 */
public class DAOConnexion {

    /**
     * Permet l'ajout d'un utilisateur à la base de données.
     * @param identifiant
     * @param nom
     * @param mdp
     * @param role
     * @param spe
     * @throws SQLException
     */
    public void addUser(String identifiant, String nom, String mdp, String role, String spe) throws SQLException{
        System.out.println("\tDAOConnexion\t\t\taddUser\t\t\tOn");
        Connection conn = this.Connection();

        //Création d'une requête préparée
        String reqAdd = "INSERT INTO `m2l_Utilisateur` (`identifiant`, `nom`, `mdp`, `role`, `specialite`)\n" +
                "VALUES (?, ?, ?, ?, ?)";
        PreparedStatement pstmt = conn.prepareStatement(reqAdd);
        //remplace le ? par une donnée
        pstmt.setString(1, identifiant); // remplace le ?
        pstmt.setString(2, nom);
        pstmt.setString(3, mdp);
        pstmt.setString(4, role);
        pstmt.setString(5, spe);
        pstmt.executeUpdate();

        pstmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");

        System.out.println("\tDAOConnexion\t\t\taddUser\t\t\tOff");
    }

    /**
     * Permet la récupération de la specialité de l'utilisateur.
     * @return Renvoie une arraylist de type String contenant les spécialités.
     * @throws SQLException
     */
    public ArrayList<String> getSpeProf() throws SQLException{
        System.out.println("\tDAOConnexion\t\t\tgetSpeProf\t\t\tOn");
        ArrayList<String> speLst = new ArrayList<>();
        Connection conn = this.Connection();

        //requette pour récup les éléments de la BDD
        String reqAfficher = "SELECT nom FROM m2l_Specialite WHERE id > 1";
        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery(reqAfficher);

        //mise de ces éléments dans l'arrayList
        while (result.next()){
            speLst.add(result.getString(1));
        }

        stmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");


        System.out.println("\tDAOConnexion\t\t\tgetSpeProf\t\t\tOff");
        return speLst;
    }

    /**
     * Permet la vérification de l'utilisateur lors de la connexion.
     * @param identifiant
     * @param mdp
     * @return vrai uniquement si les identifiants sont présents dans la base de données.
     * @throws SQLException
     */
    public boolean verifUser(String identifiant, String mdp) throws SQLException{
        System.out.println("\tDAOConnexion\t\t\tverifUser\t\t\tOn");
        Boolean uCanPass = false; //par défault, on ne peux pas se connecter
        //recupére les users et mdp de la BDD
        Connection conn = this.Connection(); //récup la co à la BDD

        //Création d'une requête préparée
        String reqAfficher = "SELECT identifiant, mdp FROM m2l_Utilisateur WHERE identifiant = ?";
        PreparedStatement pstmt = conn.prepareStatement(reqAfficher);
        //remplace le ? par une donnée
        pstmt.setString(1, identifiant); // remplace le ?
        ResultSet result = pstmt.executeQuery();

        ArrayList<UtilisateurIdentification> lst = new ArrayList<UtilisateurIdentification>();

        while(result.next()){ //remplit l'arraylist avec le résultat de la requette
            UtilisateurIdentification u = new UtilisateurIdentification(result.getString(1), result.getString(2)); //instancie un UtilisateurIdentification
            lst.add(u); //l'ajoute à la liste
            System.out.println(u);

        }

        pstmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");

        //vérifie que l'arrayList n'est pas null, et qu'il y a donc bien au moins un utilisateur dedant
        if (lst.size() == 1){
            if (lst.get(0).getMdp().equals(mdp)){ //compare le mdp donné avec celui dans la liste
                uCanPass = true;
            } else {
                System.err.println("\tDAOConnexion\t\t\tLe mot de passe est incorrect");
            }
        } //recherche dans l'arrayList le mdp
        else if (lst.size() > 1){
            for(UtilisateurIdentification u : lst) {
                if (u.getMdp().equals(mdp)){ //compare le mdp donné avec celui dans la liste
                    uCanPass = true;
                    break;
                }
            }
            if (!uCanPass){
                System.err.println("\tDAOConnexion\t\t\tLe mot de passe est incorrect");
            }
        } else {
            System.err.println("\tDAOConnexion\t\t\tLe nom d'utilisateur est incorrect");
        }
        System.out.println("\tDAOConnexion\t\t\tverifUser\t\t\tOff");
        return uCanPass;
    }

    /**
     * Permet de récupérer les informations de l'utilisateur qui est connecté.
     * @param identifiant
     * @param mdp
     * @return
     * @throws SQLException
     */
    public Utilisateur getUserIdentifier(String identifiant, String mdp) throws SQLException{
        System.out.println("\tDAOConnexion\t\t\tgetUserIdentifier\t\tOn");
        Utilisateur userIdentifier = new Utilisateur();
        //recupére les users et mdp de la BDD
        Connection conn = this.Connection(); //récup la co à la BDD

        //Création d'une requête préparée
        String reqAfficher = "SELECT id, identifiant, nom, mdp, role, specialite FROM m2l_Utilisateur WHERE identifiant = ? AND mdp = ?";
        PreparedStatement pstmt = conn.prepareStatement(reqAfficher);
        //remplace le ? par une donnée
        pstmt.setString(1, identifiant); // remplace le ?
        pstmt.setString(2, mdp);
        ResultSet result = pstmt.executeQuery();

        if (result.next()) { //récup les infos de l'utilisateur qui deviendra par la suite l'utilisateur connecté
            userIdentifier.setId(result.getInt(1));
            userIdentifier.setIdentifiant(result.getString(2));
            userIdentifier.setNom(result.getString(3));
            userIdentifier.setMdp(result.getString(4));
            userIdentifier.setRole(result.getString(5));
            userIdentifier.setSpecialite(result.getString(6));
        }

        pstmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");

        System.out.println("\tDAOConnexion\t\t\tgetUserIdentifier\t\tOff");
        return userIdentifier;
    }

    /**
     * Permet d'obtenir une liste de réservation à une date et une salle donnée.
     * @param date
     * @param salleId
     * @return
     * @throws SQLException
     */
    public ArrayList<Reservation> getReservationByDate(LocalDate date, int salleId) throws SQLException{
        System.out.println("\tDAOConnexion\t\t\tgetReservationByDate\t\tOn");
        ArrayList<Reservation> lesReservations = new ArrayList<Reservation>();

        Connection conn = this.Connection();
        //Création d'une requête préparée
        String reqAfficher = "SELECT m2l_Reservation.id, heureDebut, heureFin, id_salle, m2l_Utilisateur.nom, commentaire, jour \n" +
                "FROM m2l_Reservation \n" +
                "INNER JOIN m2l_Utilisateur ON m2l_Reservation.id_utilisateur = m2l_Utilisateur.id\n" +
                "WHERE jour = ? AND id_salle = ?";
        PreparedStatement pstmt = conn.prepareStatement(reqAfficher);
        //remplace le ? par une donnée
        pstmt.setString(1, date.toString()); // remplace le ?
        pstmt.setInt(2, salleId);
        ResultSet result = pstmt.executeQuery();

        while(result.next()){

            Reservation r = new Reservation(result.getInt(1), result.getInt(4),
                    result.getString(5), result.getInt(2), result.getInt(3),
                    result.getString(6), result.getDate(7));

            lesReservations.add(r);
        }
        System.out.println(lesReservations);

        pstmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");
        System.out.println("\tDAOConnexion\t\t\tgetReservationByDate\t\tOff");
        return lesReservations;
    }

    /**
     * Permet d'obtenir une liste de réservation à une date, une salle et une heure donnée.
     * @param date
     * @param salleId
     * @param heure
     * @return
     * @throws SQLException
     */
    public ArrayList<Reservation> getReservationByDate(LocalDate date, int salleId, int heure) throws SQLException{
        System.out.println("\tDAOConnexion\t\t\tgetReservationByDate\t\tOn");
        ArrayList<Reservation> lesReservations = new ArrayList<Reservation>();

        Connection conn = this.Connection();
        //Création d'une requête préparée
        String reqAfficher = "SELECT m2l_Reservation.id, heureDebut, heureFin, id_salle, m2l_Utilisateur.nom, commentaire, jour \n" +
                "FROM m2l_Reservation INNER JOIN m2l_Utilisateur ON m2l_Reservation.id_utilisateur = m2l_Utilisateur.id \n" +
                "WHERE jour = ? AND id_salle = ? AND heureDebut = ?";
        PreparedStatement pstmt = conn.prepareStatement(reqAfficher);
        //remplace le ? par une donnée
        pstmt.setString(1, date.toString()); // remplace le ?
        pstmt.setInt(2, salleId);
        pstmt.setInt(3, heure);
        ResultSet result = pstmt.executeQuery();

        if(result.next()){

            Reservation r = new Reservation(result.getInt(1), result.getInt(4),
                    result.getString(5), result.getInt(2), result.getInt(3),
                    result.getString(6), result.getDate(7));

            lesReservations.add(r);
        }
        //System.out.println(lesReservations);

        pstmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");
        System.out.println("\tDAOConnexion\t\t\tgetReservationByDate\t\tOff");
        return lesReservations;
    }

    /**
     * Permet de récupérer toutes les salles présentes dans la base de données.
     * @return
     * @throws SQLException
     */
    public ArrayList<Salle> getAllSalle() throws SQLException{
        System.out.println("\tDAOConnexion\t\t\tgetAllSalle\t\t\t\tOn");
        Connection conn = this.Connection();
        ArrayList<Salle> arraySalle = new ArrayList<Salle>();

        //Création d'une requête préparée
        String reqAfficher = "SELECT id, nom FROM m2l_Salle";
        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery(reqAfficher);

        while (result.next()){
            Salle s = new Salle(result.getInt(1), result.getString(2));
            arraySalle.add(s);
        }

        stmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");
        System.out.println("\tDAOConnexion\t\t\tgetAllSalle\t\t\t\tOff");
        return arraySalle;
    }

    /**
     * Permet d'ajouter une réservation dans la base de données.
     * @param res la réservation qui sera rajoutée dans la base de données.
     * @throws SQLException
     */
    public void insertReservation(Reservation res) throws SQLException{
        System.out.println("\tDAOConnexion\t\t\tinsertReservation\t\t\t\tOn");
        Connection conn = this.Connection();

        String req = "INSERT INTO `m2l_Reservation` (`heureDebut`, `heureFin`, `id_salle`, `id_utilisateur`, `commentaire`, `jour`)\n" +
                "VALUES (?, ?, ?, ?, ?, ?);";

        PreparedStatement pstmt = conn.prepareStatement(req);
        pstmt.setInt(1, res.getDebutHeure());
        pstmt.setInt(2, res.getFinHeure());
        pstmt.setInt(3, CalendrierChoix.salleSelection.getId());
        pstmt.setInt(4, Main.connectedUser.getId());
        pstmt.setString(5, res.getCommentaire());
        pstmt.setObject(6, res.getJour());

        pstmt.executeUpdate();

        pstmt.close();
        conn.close();
        System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOff");
        System.out.println("\tDAOConnexion\t\t\tinsertReservation\t\t\t\tOff");
    }

    /**
     * Permet la connexion à la base de données.
     * @return de type Connection
     */
    private Connection Connection(){
        Connection conn = null;
        try{
            String urlBDD = "jdbc:mysql://sio-hautil.eu:3306/jameta?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

            String user = "jameta";
            String pwd = "antonin";

            conn = DriverManager.getConnection(urlBDD, user, pwd);
            System.out.println("\tDAOConnexion\t\t\tConnexion à la BDD\t\tOn");

        } catch (SQLException e){
            System.err.println("La connexion à la BDD n'a pas pu aboutir");
            ReservationController.status = 4;
            Stage err = new Stage();
            ReservationController res = new ReservationController();
            res.popup(err); //lance la popup en cas d'erreur
            throw e;
        } finally {
            return conn;
        }
    }
}