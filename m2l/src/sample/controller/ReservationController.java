package sample.controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import sample.Main;
import sample.objet.DAOConnexion;
import sample.objet.Reservation;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

/**
 * Il s'agit du controller relatif à la page FXML de Reservation de l'application.
 * Permet la réservation d'une salle à une horaire et une date donnée.
 */
public class ReservationController implements Initializable {

    @FXML
    Label connectionLabel, resSalle, comCompteLbl;

    @FXML
    Button retourBtn;

    @FXML
    ComboBox<Integer> heureDispoCbox;

    @FXML
    DatePicker datePick;

    @FXML
    TextArea commentaireArea;

    /**
     * Permet de retourner à la page précédente.
     * @param event l'action du bouton
     */
    @FXML
    private void retour(ActionEvent event){

        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) retourBtn.getScene().getWindow();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/calendrierChoix.fxml"));
            System.out.println("CalendrierChoix.java\tretourBtn actionné\t\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Choix de la Salle");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur menu.fxml");
            e.printStackTrace();
        }
    }

    public static int status;
    //public int getStatus(){return status;}

    /**
     * Création de la reservation dans la base de données en récupérant l'utilisateur, le commentaire, la date, de l'heure et de la date sélectionnée.
     */
    @FXML
    public void reserver(){
        System.out.println("CalendrierChoix.java\treserverBtn actionné\t\tok");

        LocalDate ldate = datePick.getValue();
        Stage popup = new Stage();

        if(ldate != null){
            if (heureDispoCbox.getSelectionModel().getSelectedItem() != null){
                int debutHeure = heureDispoCbox.getSelectionModel().getSelectedItem();
                int salleID = CalendrierChoix.salleSelection.getId();
                int userID = Main.connectedUser.getId();

                String comm = commentaireArea.getText();

                Date date = this.convertToDateUsingDate(ldate);

                Reservation res = new Reservation(salleID, userID, debutHeure, comm, date);

                try{
                    DAOConnexion dao = new DAOConnexion();
                    dao.insertReservation(res);
                    //connectionLabel.setText("Enregistrement effectué");
                    status = 1;
                    System.out.println(status);
                    popup(popup);
                } catch (SQLException e){
                    System.err.println("Problème lors de l'enregistrement");
                    status = 0;
                    System.out.println(status);
                    popup(popup);
                    //connectionLabel.setText("Problème lors de l'enregistrement");
                    e.printStackTrace();
                }
            } else {
                //connectionLabel.setText("Veuillez choisir une heure");
                status = 2;
                System.out.println(status);
                popup(popup);
            }
        } else {
            //connectionLabel.setText("Veuillez choisir une date");
            status = 3;
            System.out.println(status);
            popup(popup);
        }

    }

    public static FXMLLoader loader;

    public void popup(Stage popup) {
        popup.setTitle("Information");
        popup.initModality(Modality.APPLICATION_MODAL); //bloque l'utilisation de l'appli tant que ouvert
        popup.setResizable(false);
        try {
            loader = new FXMLLoader(getClass().getResource("/fxml/pop.fxml"));
            Parent rootParent = loader.load();
            Scene rootScene = new Scene(rootParent);

            popup.setScene(rootScene);
            popup.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static Stage pStage;
    public static Stage getpStage(){return pStage;}


    public void clean(){

        heureDispoCbox.setItems(null);
        datePick.setValue(null);
        commentaireArea.clear();

    }

    /**
     * Converti une variable de type LocalDate en Date.
     * @param date la LocalDate a convertir.
     * @return variable de type Date.
     */
    //https://java2blog.com/java-localdate-to-date/
    private Date convertToDateUsingDate(LocalDate date) {
        return java.sql.Date.valueOf(date);
    }

    /**
     * Affiche uniquement les heures disponibles à une date et une salle donnée.
     */
    @FXML
    public void afficheHeure(){
        LocalDate ld = datePick.getValue();

        if(ld != null){

            //System.out.println(CalendrierChoix.salleSelection.getId() + " " + CalendrierChoix.salleSelection.getNom());
            try {
                DAOConnexion dao = new DAOConnexion();
                //récup les heures utiliser
                //en déduit les heures libres
                ArrayList<Integer> heureLibreLst = calculHeureLibre(dao.getReservationByDate(ld, CalendrierChoix.salleSelection.getId()));
                //mettre la liste dans la ComboBox
                ObservableList<Integer> nomSalleObs = FXCollections.observableArrayList(heureLibreLst);
                heureDispoCbox.setItems(nomSalleObs);
                heureDispoCbox.getSelectionModel().select(0);
            } catch (SQLException e){
                e.printStackTrace();
            }
        } else {
            connectionLabel.setText("Veuillez choisir une date");
            connectionLabel.setVisible(true);
        }
    }

    /**
     * Récupère les reservations et les heures déjà réservées pour un jour et une salle donnée et renvoie la liste des heure disponibles.
     * @param lstHeureOccuper Les heures déjà réservée, et donc inutilisable
     * @return Les heures qui nes sont pas utilisé, et donc utilisable
     */
    private ArrayList<Integer> calculHeureLibre(ArrayList<Reservation> lstHeureOccuper){
        //set toutes les heures potentiellement disponibles
        ArrayList<String> lstString = new ArrayList<>();
        lstString.add("7");
        lstString.add("8");
        lstString.add("9");
        lstString.add("10");
        lstString.add("11");
        lstString.add("12");
        lstString.add("13");
        lstString.add("14");
        lstString.add("15");
        lstString.add("16");
        lstString.add("17");
        lstString.add("18");

        //enlève des heures libres les heures occupées
        for (Reservation res : lstHeureOccuper){
            int numAdelete = res.getDebutHeure();

            String stNumAdelete = String.valueOf(numAdelete);
            String receptacle = null;
            for (String carr : lstString){
                if(carr.equals(stNumAdelete)){
                    receptacle = carr;
                }
            }
            if(receptacle != null){
                lstString.remove(receptacle);
            }
        }
        System.out.println("liste des heures disponibles: " + lstString.toString());

        //converti la lst de string en lst d'int
        return getIntegerArray(lstString);
    }

    /**
     * Convertir l'arraylist de type String en arraylist de type Int.
     * @param stringArray ArrayList à convertire
     * @return ArrayList convertie
     */
    //https://stackoverflow.com/questions/7708698/convert-arrayliststring-to-an-arraylistinteger-or-integer-array
    private ArrayList<Integer> getIntegerArray(ArrayList<String> stringArray) {
        ArrayList<Integer> result = new ArrayList<>();
        for(String stringValue : stringArray) {
            try {
                //Convert String to Integer, and store it into integer array list.
                result.add(Integer.parseInt(stringValue));
            } catch(NumberFormatException nfe) {
                System.out.println("Could not parse " + nfe);
            }
        }
        return result;
    }

    /**
     * Affiche l'utilisateur connecté si il est connecté et affiche son role et sa spécialité.
     * N'affiche rien si on n'est pas authentifié.
     * Permet d'afficher la salle sélectionnée
     * Indique à l'utiisateur le nombre de caractères restants dans le commentaire.
     * @param url implémentation automatique, pas utilisé
     * @param resourceBundle implémentation automatique, pas utilisé
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //affiche l'utilisateur connecté s'il y en a un
        if (Main.connectedUser != null){
            connectionLabel.setText(Main.connectedUser.getNom() + " " + Main.connectedUser.getRole() + " " + Main.connectedUser.getSpecialite());
            connectionLabel.setVisible(true);
        }

        //affiche le nom de la salle sélectionnée
        resSalle.setText("Réservation " + CalendrierChoix.salleSelection.getNom());

        //binding des heures disponibles
        datePick.valueProperty().addListener((event) -> afficheHeure());

        //set le compte de la zone de commentaire, afin de prévenir l'utilisateur de s'il a atteint la limite
        commentaireArea.textProperty().addListener((event) -> {
            comCompteLbl.setText(commentaireArea.lengthProperty().get() + "/100");
            if (commentaireArea.lengthProperty().get() > 100){
                comCompteLbl.setTextFill(Color.RED);
            } else if (commentaireArea.lengthProperty().get() < 101){
                comCompteLbl.setTextFill(Color.BLACK);
            }
        });
    }
}
