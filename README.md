# Projet M2L - Dilan et Antonin

## Rappel des contraintes techniques :

* Lanagage utilisé : Java
* Framework de présentation : JavaFX
* Données : BDD SQL accessible sur le Net (exemple: MariaDB de SIO-Hautil)
* Gestion du code : via Git sur un projet dédié partagé sur GitLab.com

## Contexte

Le groupe **La Maison des Ligues de Lorraine** à du mal à gérer les réservations de ses différentes salles. Il arrive en effet souvent que plusieurs professeurs réservent le même créneau horaire d'une salle, causant des problèmes. Il nous est donc demandé de leur créer une application qui leur permettra de facilement gérer leurs différentes salles. 

## Étape 1 - en cours

Créer un **cahier des charges**, un **diagramme de classe** et un **diagramme de cas d'utilisation**. Ils devront décrire comment on répondra au problème et quelles seront les fonctionnalités de l'application.
