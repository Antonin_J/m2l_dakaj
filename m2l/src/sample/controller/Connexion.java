package sample.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import sample.Main;
import sample.objet.DAOConnexion;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Il s'agit du controller relatif à la page FXML de Connexion de l'application.
 * Permet l'authentification d'un utilisateur à l'application avec un nom d'utilisateur et un mot de passe.
 */
public class Connexion implements Initializable {

    @FXML
    Label connectionLabel;

    @FXML
    Button retourBtn, connexionBtn;

    @FXML
    TextField userField, mdpField;

    /**
    *   Permet de retourner à la page précédente.
    * @param event
     */
    @FXML
    private void retour(ActionEvent event){

        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) retourBtn.getScene().getWindow();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/menu.fxml"));
            System.out.println("Connexion.java\t\t\tretourBtn actionné\t\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Menu");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur menu.fxml");
            e.printStackTrace();
        }
    }

    /**
     * Fonction qui s'éxécute lorsque l'on clique sur le bouton "se connecter".
     * Compare les identifiants du formulaire avec ceux dans la base de données et vérifie donc si ils existent.
     * Pour ensuite valider ou non l'identification.
     * @param calevent
     */
    @FXML
    private void connexion(ActionEvent calevent){
        System.out.println("Connexion.java\t\t\tconnexionBtn actionné\tvérification TODO");

        DAOConnexion daoConn = new DAOConnexion();
        try{//vérifier les identifiants rentré par l'utilisateur
            if (daoConn.verifUser(userField.getText(), mdpField.getText())){
                try {//récupére dans la BDD les infos de l'utilisateur et l'identifie dans le logiciel
                    Main.connectedUser = daoConn.getUserIdentifier(userField.getText(), mdpField.getText());
                } catch (SQLException e){
                    System.err.println("DAO_connxeion verifUser -> getUserIdentifier problème");
                    e.printStackTrace();
                }

                //permet de changer de fichier FXML
                Stage stage;
                Parent root;
                //probablement récupére le stage en cours d'utilisation (primaryStage)
                stage = (Stage) connexionBtn.getScene().getWindow();
                try {
                    root = FXMLLoader.load(getClass().getResource("/fxml/menu.fxml"));
                    System.out.println("Connexion.java\t\t\tvérification des ID\t\tOK");
                    stage.setTitle("Maison des Ligues de Loraine - Menu");
                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException e){
                    System.err.println("FXMLLoader.load fait une erreur menu.fxml");
                    e.printStackTrace();
                }
            } else {
                connectionLabel.setText("Le nom d'utilisateur ou le mot de passe est incorrect !");
                connectionLabel.setVisible(true); //permet de le rendre visible car il ne l'est pas par défaut
            }
        } catch (SQLException e){
            System.err.println("DAO_connxeion verifUser problème");
            e.printStackTrace();
        }

    }

    /**
     * Permet à l'utilateur de valider les champs grace à la touche entrée.
     * Fonctionne seulement si nous sommes sur le champs du mot de passe.
     * @param event
     */
    @FXML
    public void connexionKey(KeyEvent event){
        if (event.getCode() == KeyCode.ENTER){ //vérifie que c'est sur la touche entrer
            ActionEvent aevent = new ActionEvent();
            this.connexion(aevent);
        }
    }

    /**
     * Affiche l'utilisateur connecté si il est connecté et affiche son role et sa spécialité.
     * N'affiche rien si on n'est pas authentifié.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //affiche l'utilisateur connecté s'il y en a un
        if (Main.connectedUser != null){
            connectionLabel.setText(Main.connectedUser.getNom() + " " + Main.connectedUser.getRole() + " " + Main.connectedUser.getSpecialite());
            connectionLabel.setVisible(true);
        }
    }
}
