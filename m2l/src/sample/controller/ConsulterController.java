package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.Main;
import sample.objet.DAOConnexion;
import sample.objet.Reservation;

import javax.swing.plaf.PanelUI;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Il s'agit du controller relatif à la page FXML de consulter de l'application.
 * Permet de consulter les reservations d'une salle donnée avec des paramètres: Le jour et l'horaire.
 */
public class ConsulterController implements Initializable {

    @FXML
    Label resSalle, connectionLabel;

    @FXML
    ComboBox<String> heureDispoCbox;

    @FXML
    DatePicker datePick;

    @FXML
    Button consulterBtn, retourBtn, reserverBtn;

    @FXML
    TableView mainTableView;

    @FXML
    TableColumn colHeureD, colHeureF, colProf, colComm;

    /**
     *   Permet de retourner à la page précédente.
     * @param event
     */
    @FXML
    public void retour(ActionEvent event){
        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) retourBtn.getScene().getWindow();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/calendrierChoix.fxml"));
            System.out.println("ConsulterController.java\tretourBtn actionné\t\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Choix de la Salle");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur calendrierChoix.fxml");
            e.printStackTrace();
        }
    }

    public static FXMLLoader loader;
    /**
     * Permet de passer de la page Consulter à la page Reserver.
     * Accessible seulement pour un utilisateur authentifié.
     */
    @FXML
    public void reserverSwitch(){
        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) retourBtn.getScene().getWindow();
        try {
            loader = new FXMLLoader(getClass().getResource("/fxml/reservation.fxml"));
            root = loader.load();
            System.out.println("ConsulterController.java\treserverSwitch actionné\t\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Reservation");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur reservation.fxml");
            e.printStackTrace();
        }
    }

    /**
     * Affiche en fonction des champs remplis les réservations correspondants au critères de recherche et de le salle.
     */
    @FXML
    public void afficheReservation(){
        System.out.println("ConsulterController.java\tafficheReservation actionné\t\tTODO");

        connectionLabel.setVisible(false);
        mainTableView.setItems(null);

        if (datePick.getValue() != null && heureDispoCbox.getSelectionModel().getSelectedIndex() == 0){
            //System.out.println("Date choisie " + datePick.getValue());
            //récup les réservations
            DAOConnexion dao = new DAOConnexion();
            ArrayList<Reservation> reservationsArr = null;
            try {
                reservationsArr = dao.getReservationByDate(datePick.getValue(), CalendrierChoix.salleSelection.getId());
            } catch (SQLException e){
                System.err.println("Problème lors du chargement des réservations");
                e.printStackTrace();
                connectionLabel.setText("Problème lors du chargement des réservations");
                connectionLabel.setVisible(true);
            }

            if (reservationsArr.size() != 0){
                //associe une colonne avec une caractéristique de l'objet Reservation
                colHeureD.setCellValueFactory(new PropertyValueFactory<>("debutHeure"));
                colHeureF.setCellValueFactory(new PropertyValueFactory<>("finHeure"));
                colProf.setCellValueFactory(new PropertyValueFactory<>("userNom"));
                colComm.setCellValueFactory(new PropertyValueFactory<>("Commentaire"));

                ObservableList<Reservation> obsRes = FXCollections.observableArrayList(reservationsArr);
                mainTableView.setItems(obsRes);

            }else {
                connectionLabel.setText("Aucune réservation pour cette date");
                connectionLabel.setVisible(true);
            }

        } else if (datePick.getValue() != null && heureDispoCbox.getSelectionModel().getSelectedIndex() != 0){
            //System.out.println("Date choisie " + datePick.getValue() + " heure choisie " + heureDispoCbox.getValue());

            //récup les réservations
            DAOConnexion dao = new DAOConnexion();
            ArrayList<Reservation> reservationsArr = null;

            try {
                reservationsArr = dao.getReservationByDate(datePick.getValue(), CalendrierChoix.salleSelection.getId(),
                        Integer.valueOf(heureDispoCbox.getSelectionModel().getSelectedItem()));
            } catch (SQLException e){
                System.err.println("Problème lors du chargement des réservations");
                e.printStackTrace();
                connectionLabel.setText("Problème lors du chargement des réservations");
                connectionLabel.setVisible(true);
            }



            if (reservationsArr.size() > 0){
                //associe une colonne avec une caractéristique de l'objet Reservation
                colHeureD.setCellValueFactory(new PropertyValueFactory<>("debutHeure"));
                colHeureF.setCellValueFactory(new PropertyValueFactory<>("finHeure"));
                colProf.setCellValueFactory(new PropertyValueFactory<>("userNom"));
                colComm.setCellValueFactory(new PropertyValueFactory<>("Commentaire"));

                ObservableList<Reservation> obsRes = FXCollections.observableArrayList(reservationsArr);
                mainTableView.setItems(obsRes);

            } else {

                connectionLabel.setText("Aucune réservation pour cette date à cette heure");
                connectionLabel.setVisible(true);
            }

        } else{
            connectionLabel.setText("Veuillez choisir une date");
            connectionLabel.setVisible(true);
        }

    }

    /**
     * Affiche l'utilisateur connecté si il est connecté et affiche son role et sa spécialité.
     * N'affiche rien si on n'est pas authentifié.
     * <p>
     * Initialise la liste complète des heures diponibles.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //affiche l'utilisateur connecté s'il y en a un
        if (Main.connectedUser != null){
            connectionLabel.setText(Main.connectedUser.getNom() + " " + Main.connectedUser.getRole() + " " + Main.connectedUser.getSpecialite());
            connectionLabel.setVisible(true);
            reserverBtn.setVisible(true);
        }

        //affiche le nom de la salle sélectionnée
        resSalle.setText("Salle " + CalendrierChoix.salleSelection.getNom());

        ArrayList<String> heureArr = new ArrayList<>();
        heureArr.add("Heures");
        heureArr.add("7");
        heureArr.add("8");
        heureArr.add("9");
        heureArr.add("10");
        heureArr.add("11");
        heureArr.add("12");
        heureArr.add("13");
        heureArr.add("14");
        heureArr.add("15");
        heureArr.add("16");
        heureArr.add("17");
        heureArr.add("18");

        ObservableList<String> obsHeure = FXCollections.observableArrayList(heureArr);

        heureDispoCbox.setItems(obsHeure);
        heureDispoCbox.getSelectionModel().select(0);

    }
}
