package sample.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.Main;
import sample.objet.DAOConnexion;
import sample.objet.Salle;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Il s'agit du controller relatif à la page FXML de CalendrierChoix de l'application.
 * Permet de choisir une salle pour laquelle on va afficher le calendrier ou reserver.
 */
public class CalendrierChoix implements Initializable {

    @FXML
    Button retourBtn, consulterBtn;

    @FXML
    ComboBox lstSalleCbox;

    @FXML
    Label connectionLabel;


    /**
     * Enregistre la salle sélectionnée par l'utilisateur.
     */
    public static Salle salleSelection;


    /**
     * Permet de retourner à la page précédente.
     * @param event
     */
    @FXML
    private void retour(ActionEvent event){

        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) retourBtn.getScene().getWindow();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/menu.fxml"));
            System.out.println("CalendrierChoix.java\tretourBtn actionné\t\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Menu");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur menu.fxml");
            e.printStackTrace();
        }
    }

    /**
     * Permet de passer à la page suivante lorsque l'utilisateur a choisi la salle recherchée.
     */
    @FXML
    private void switchCalendrierBtn(){
        System.out.println("CalendrierChoix.java\tconsulterBtn actionné\t\tOk");

        this.salleSelection = ((Salle) lstSalleCbox.getSelectionModel().getSelectedItem());
        System.out.println(salleSelection.getId() + " " + salleSelection.getNom());

        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) retourBtn.getScene().getWindow();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/consulter.fxml"));
            System.out.println("CalendrierChoix.java\tconsulterBtn actionné\t\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Calendrier");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur calendrierChoix.fxml");
            e.printStackTrace();
        }
    }

    /**
     * Affiche l'utilisateur connecté si il est connecté et affiche son role et sa spécialité.
     * N'affiche rien si on n'est pas authentifié.
     * Récupère ensuite les salles présentes dans la base de données pour la mettre dans une Combobox.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //affiche l'utilisateur connecté s'il y en a un
        if (Main.connectedUser != null){
            connectionLabel.setText(Main.connectedUser.getNom() + " " + Main.connectedUser.getRole() + " " + Main.connectedUser.getSpecialite());
            connectionLabel.setVisible(true);
        }

        DAOConnexion dao = new DAOConnexion();
        //set la comboBox
        ArrayList<Salle> nomSalleArray = new ArrayList<>();
        try {
            nomSalleArray = dao.getAllSalle();
        } catch (SQLException e){
            System.err.println("Problème lors du chargement des salles");
            e.printStackTrace();
        }

        ObservableList<Salle> nomSalleObs = FXCollections.observableArrayList(nomSalleArray);

        lstSalleCbox.setItems(nomSalleObs);
        lstSalleCbox.getSelectionModel().select(0);
    }
}
