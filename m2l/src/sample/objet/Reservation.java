package sample.objet;

import java.util.Date;

/**
 * Objet de type Reservation
 */
public class Reservation {
    private int salleId, userID, finHeure, debutHeure, id;
    private String commentaire, userNom;
    private Date jour;

    /**
     * Constructeur Reservation
     * @param id
     * @param salleId
     * @param userNom
     * @param debutHeure
     * @param finHeure
     * @param commentaire
     * @param jour
     */
    public Reservation (int id, int salleId, String userNom, int debutHeure, int finHeure, String commentaire, Date jour){
        this.id = id;
        this.salleId = salleId;
        this.userNom = userNom;
        this.debutHeure = debutHeure;
        this.finHeure = finHeure;
        this.commentaire = commentaire;
        this.jour = jour;
    }

    /**
     * Constructeur Reservation
     * @param salleId
     * @param userID
     * @param debutHeure
     * @param commentaire
     * @param jour
     */
    public Reservation (int salleId, int userID, int debutHeure, String commentaire, Date jour){
        this.salleId = salleId;
        this.userID = userID;
        this.debutHeure = debutHeure;
        this.finHeure = debutHeure + 1;
        this.commentaire = commentaire;
        this.jour = jour;
    }

    /**
     * Accesseur permettant de récupérer l'id de l'utilisateur
     * @return
     */
    public int getUserID() {
        return userID;
    }

    public String getUserNom() {return userNom;}

    /**
     * Accesseur permettant de récupérer l'heure de fin
     * @return
     */
    public int getFinHeure() {return finHeure;}

    /**
     * Accesseur permettant de récupérer l'heure de début
     * @return
     */
    public int getDebutHeure() {return debutHeure;}

    /**
     * Accesseur permettant de récupérer l'id de la réservation
     * @return
     */
    public int getId() {return id;}

    /**
     * Accesseur permettant de récupérer le commentaire
     * @return
     */
    public String getCommentaire() {return commentaire;}

    /**
     * Accesseur permettant de récupérer le jour
     * @return
     */
    public Date getJour() {return jour;}

    /**
     * Réécrit la méthode toString de l'objet Reservation
     * @return
     */
    @Override
    public String toString() {
        return "Reservation{" +
                "salleId=" + salleId +
                ", userID=" + userID +
                ", userNom=" + userNom +
                ", finHeure=" + finHeure +
                ", debutHeure=" + debutHeure +
                ", id=" + id +
                ", commentaire='" + commentaire + '\'' +
                ", jour=" + jour +
                '}';
    }
}
