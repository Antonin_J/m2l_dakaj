package sample.objet;

/**
 * Objet de type Role
 */
public class Role {
    private int id;
    private String nom;

    /**
     * Récupère l'id de l'objet
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Permet la modification de l'id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Récupère le nom de l'objet
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet la modification du nom
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Réécrit la méthode toString de l'objet Role
     * @return
     */
    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                '}';
    }
}
