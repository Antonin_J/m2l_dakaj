package sample.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Il s'agit du controller relatif à la page FXML de Menu de l'application.
 */
public class Menu implements Initializable {


    @FXML //load les objets graphiques (node) du fichier FXML
    Button exitBtn, calendrierBtn, connexionBtn, creationBtn;

    @FXML
    Label connectionLabel;


    /**
     * Permet de changer de page vers Inscription si l'utilisateur est un administrateur.
     */
    @FXML
    private void switchCreationUser(){
        System.out.println("Menu.java\t\t\t\tcreationBtn actionné\tOK");


        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) connexionBtn.getScene().getWindow();

        try {
            ConsulterController.loader = new FXMLLoader(getClass().getResource("/fxml/inscription.fxml"));
            root = ConsulterController.loader.load();
            System.out.println("Menu.java\t\t\t\tcreationBtn actionné\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Création d'utilisateur");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur inscription.fxml");
            e.printStackTrace();
        }
    }


    /**
     * Permet de changer de page vers CalendrierChoix
     * @param calevent
     */
    @FXML
    private void switchCalendrierBtn(ActionEvent calevent){
        //permet de changer de fichier FXML
        Stage stage;
        Parent root;


        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) connexionBtn.getScene().getWindow();

        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/calendrierChoix.fxml"));
            System.out.println("Menu.java\t\t\t\tcalendrierBtn actionné\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Choix de la Salle");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur calendrierChoix.fxml");
            e.printStackTrace();
        }



    }

    /**
     * Permet de changer de page vers Connexion
     * @param actionEvent
     */
    @FXML
    public void switchConnexionBtn(ActionEvent actionEvent){
        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) exitBtn.getScene().getWindow();

        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/connexion.fxml"));
            System.out.println("Menu.java\t\t\t\tconnexionBtn actionné\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Connexion");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur connexion.fxml");
            e.printStackTrace();
        }
    }

    /**
     * Ferme l'application.
     * @param event
     */
    @FXML
    private void exitBtn(ActionEvent event){

        System.out.println("L'application se ferme");
        Platform.exit();

    }

    /**
     * Affiche l'utilisateur connecté si il est connecté et affiche son role et sa spécialité.
     * N'affiche rien si on n'est pas authentifié.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //si on veux initialiser des choses avant de charger le fichier FXML par exemple
        //affiche l'utilisateur connecté s'il y en a un
        if (Main.connectedUser != null){
            connectionLabel.setText(Main.connectedUser.getNom() + " " + Main.connectedUser.getRole() + " " + Main.connectedUser.getSpecialite());
            connectionLabel.setVisible(true);

            //affiche le bouton si l'utilisateur est admin
            if (Main.connectedUser.getRole().equals("Admin")){
                creationBtn.setVisible(true);
            }
        }

    }

}
