package sample.objet;

/**
 * Objet de type Utilisateur
 */
public class Utilisateur {
    private int id;
    private String identifiant;
    private String nom;
    private String mdp;
    private String role;
    private String specialite;

    /**
     * Constructeur Utilisateur
     */
    public Utilisateur(){
        setIdentifiant(null);
        setMdp(null);
        setNom(null);
    }

    /**
     * Constructeur Utilisateur
     */
    public Utilisateur(int id, String iden, String nom, String mdp,String role, String spe){
        setId(id);
        setIdentifiant(iden);
        setMdp(nom);
        setNom(mdp);
        setRole(role);
        setSpecialite(spe);
    }

    /**
     * Constructeur Utilisateur
     */
    public Utilisateur(String iden, String nom, String mdp,String role, String spe){
        setIdentifiant(iden);
        setMdp(nom);
        setNom(mdp);
        setRole(role);
        setSpecialite(spe);
    }

    /**
     * Récupère le nom du role de l'utilisateur.
     * @return
     */
    public String getRole() {
        return role;
    }

    /**
     * Permet la modification du role de l'utilisateur.
     * @param role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Récupère le nom de la spécialité de l'utilisateur.
     * @return
     */
    public String getSpecialite() {
        return specialite;
    }

    /**
     * Permet la modification de la spécialité de l'utilisateur.
     * @param specialite
     */
    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    /**
     * Récupère l'id de l'utilisateur.
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Permet la modification de l'id de l'utilisateur.
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Récupère l'identifiant de l'utilisateur.
     * @return
     */
    public String getIdentifiant() {
        return identifiant;
    }

    /**
     * Permet la modification de l'identidiant de l'utilisateur.
     * @param identifiant
     */
    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    /**
     * Récupère le nom de l'utilisateur.
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     * Permet la modification du nom de l'utilisateur.
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Récupère le mot de passe de l'utilisateur.
     * @return
     */
    public String getMdp() {
        return mdp;
    }

    /**
     * Permet la modification du mot de passe de l'utilisateur.
     * @param mdp
     */
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    /**
     * Réécrit la méthode toString de l'objet Salle
     * @return
     */
    @Override
    public String toString() {
        return "Utilisateur{" +
                "id=" + id +
                ", identifiant='" + identifiant + '\'' +
                ", nom='" + nom + '\'' +
                ", mdp='" + mdp + '\'' +
                '}';
    }
}
