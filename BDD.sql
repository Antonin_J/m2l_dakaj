-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `jameta`;
CREATE DATABASE `jameta` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jameta`;

DROP TABLE IF EXISTS `m2l_Reservation`;
CREATE TABLE `m2l_Reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heureDebut` datetime NOT NULL,
  `heureFin` datetime NOT NULL,
  `id_salle` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `commentaire` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_salle` (`id_salle`),
  KEY `id_utilisateur` (`id_utilisateur`),
  CONSTRAINT `m2l_Reservation_ibfk_1` FOREIGN KEY (`id_salle`) REFERENCES `m2l_Salle` (`id`),
  CONSTRAINT `m2l_Reservation_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `m2l_Utilisateur` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `m2l_Role`;
CREATE TABLE `m2l_Role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nom` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `m2l_Role` (`id`, `nom`) VALUES
(1,	'admin'),
(2,	'Professeur');

DROP TABLE IF EXISTS `m2l_Salle`;
CREATE TABLE `m2l_Salle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(35) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `m2l_Salle` (`id`, `nom`) VALUES
(1,	'Salle Multifonction'),
(2,	'Dojo'),
(3,	'Salle d\'Escrime'),
(4,	'Salle de Danse - Gymnastique'),
(5,	'Terrain de Football'),
(6,	'Terrain de Tennis');

DROP TABLE IF EXISTS `m2l_Specialite`;
CREATE TABLE `m2l_Specialite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nom` (`nom`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `m2l_Specialite` (`id`, `nom`) VALUES
(1,	'admin'),
(9,	'Badminton'),
(2,	'Danse Classique'),
(13,	'Danse Madison'),
(4,	'Escrime'),
(6,	'Football'),
(5,	'Gymnastique'),
(8,	'Handball'),
(10,	'Judo'),
(3,	'Karaté'),
(12,	'Kung Fu'),
(11,	'Taekwondo'),
(14,	'Tango de Salon'),
(7,	'Tennis'),
(15,	'Valse');

DROP TABLE IF EXISTS `m2l_Utilisateur`;
CREATE TABLE `m2l_Utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifiant` varchar(20) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `role` int(11) NOT NULL,
  `specialite` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`),
  KEY `specialite` (`specialite`),
  CONSTRAINT `m2l_Utilisateur_ibfk_1` FOREIGN KEY (`role`) REFERENCES `m2l_Role` (`id`),
  CONSTRAINT `m2l_Utilisateur_ibfk_2` FOREIGN KEY (`specialite`) REFERENCES `m2l_Specialite` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `m2l_Utilisateur` (`id`, `identifiant`, `nom`, `mdp`, `role`, `specialite`) VALUES
(1,	'admin',	'Dév',	'admin',	1,	1);

-- 2020-12-06 13:47:52
