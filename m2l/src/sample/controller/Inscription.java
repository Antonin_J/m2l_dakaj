package sample.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import sample.Main;
import sample.objet.DAOConnexion;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Il s'agit du controller relatif à la page FXML de Inscription de l'application.
 * Permet d'inscrire un nouvel utilisateur professeur ou administrateur.
 */
public class Inscription implements Initializable {

    @FXML
    Label connectionLabel;

    @FXML
    TextField nomUserField, mdpUserField, idUserField;

    @FXML
    ComboBox<String> roleCbox, specialiterCbox;

    @FXML
    Button creationBtn, retourBtn;

    DAOConnexion daoConn = new DAOConnexion();

    /**
     *   Permet de retourner à la page précédente.
     * @param event
     */
    @FXML
    private void retour(ActionEvent event){

        //permet de changer de fichier FXML
        Stage stage;
        Parent root;

        //probablement récupére le stage en cours d'utilisation (primaryStage)
        stage = (Stage) retourBtn.getScene().getWindow();
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/menu.fxml"));
            System.out.println("Connexion.java\t\t\tretourBtn actionné\t\tOK");
            stage.setTitle("Maison des Ligues de Loraine - Menu");
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e){
            System.err.println("FXMLLoader.load fait une erreur menu.fxml");
            e.printStackTrace();
        }
    }

    /**
     * Fonction permettant la création de nouveaux comptes.
     * @param event
     */
    @FXML
    public void creerCompte(ActionEvent event){
        System.out.println("Connexion.java\t\t\tcreationBtn actionné\t\tOK TODO");
        String r = "";
        if ( !nomUserField.getText().equals(r) && !idUserField.getText().equals(r) && !mdpUserField.getText().equals(r) ){
            try{
                daoConn.addUser(idUserField.getText(), nomUserField.getText(), mdpUserField.getText(),
                        roleCbox.getSelectionModel().getSelectedItem(), specialiterCbox.getSelectionModel().getSelectedItem());
                //connectionLabel.setText("Utilisateur "+ nomUserField.getText() +" créé correctement");
                ReservationController.status = 5;
                ReservationController res = new ReservationController();
                Stage stage = new Stage();
                res.popup(stage);
            } catch (SQLException e){
                connectionLabel.setText("Problème lors de la création de l'utilisateur: "+ nomUserField.getText());
                System.err.println("DAOConnexion addUser a fait une erreur");
                e.printStackTrace();
                ReservationController.status = 4;
                ReservationController res = new ReservationController();
                Stage stage = new Stage();
                res.popup(stage);
            }
        } else {
            ReservationController.status = 6;
            ReservationController res = new ReservationController();
            Stage stage = new Stage();
            res.popup(stage);
        }

    }

    public void clean(){
        nomUserField.clear();
        idUserField.clear();
        mdpUserField.clear();
    }

    /**
     * Permet à l'utilateur de valider les champs grace à la touche entrée.
     * Fonctionne seulement si nous sommes sur le champs du mot de passe.
     * @param event
     */
    @FXML
    public void connexionKey(KeyEvent event){
        if (event.getCode() == KeyCode.ENTER){ //vérifie que c'est sur la touche entrer
            ActionEvent aevent = new ActionEvent();
            this.creerCompte(aevent);
        }
    }

    /**
     * Initialiser la comboBox specialiterCbox avec les spécialités des profs depuis la base de données.
     */
    private void setSpecialiterCboxProf(){
        try {
            ArrayList<String> speLst = daoConn.getSpeProf();
            ObservableList speOblist = FXCollections.observableList(speLst); //transforme ArrayList en ObservableList
            specialiterCbox.getItems().clear(); //m'assure de vider la Cbox
            specialiterCbox.setItems(speOblist);
            specialiterCbox.getSelectionModel().select(0);
        } catch (SQLException e){
            System.err.println("DAOConnexion getSpeProf a fait une erreur");
            e.printStackTrace();
        }


    }

    /**
     * Affiche l'utilisateur connecté si il est connecté et affiche son role et sa spécialité.
     * N'affiche rien si on n'est pas authentifié.
     * <p>
     * Affecte les valeurs au ComboBox.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //affiche l'utilisateur connecté s'il y en a un
        if (Main.connectedUser != null){
            connectionLabel.setText(Main.connectedUser.getNom() + " " + Main.connectedUser.getRole() + " " + Main.connectedUser.getSpecialite());
            connectionLabel.setVisible(true);
        }

        //peupler la comboBox roleCbox
        roleCbox.getItems().addAll("Professeur", "Admin");
        roleCbox.getSelectionModel().select(0);

        //initialiser la comboBox specialiterCbox avec les spécialités des profs
        setSpecialiterCboxProf();

        //peupler la comboBox specialiterCbox en fonction du role choisi dans la Cbox roleCbox
        //selectedItemProperty() permet de changer dynamiquement les valeurs
        roleCbox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                if (roleCbox.getSelectionModel().getSelectedIndex() == 0) {
                    setSpecialiterCboxProf();
                } else {
                    specialiterCbox.getItems().clear(); //m'assure de vider la Cbox
                    specialiterCbox.getItems().addAll("Admin");
                }
                specialiterCbox.getSelectionModel().select(0); //préselectionne dans tout les cas le premier choix
            }
        });
    }
}
