package sample.objet;

/**
 * Objet de type Salle
 */
public class Salle {
    private int id;
    private String nom;

    /**
     * Constructeur de l'objet Salle
     * @param id
     * @param nom
     */
    public Salle(int id, String nom){
        this.id = id;
        this.nom = nom;
    }

    /**
     * Permet la modification de l'id
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Permet la modification du nom
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Accesseur permettant de récupérer l'id de la salle
     * @return
     */
    public int getId(){
        return id;
    }

    /**
     * Accesseur permettant de récupérer le nom de la salle
     * @return
     */
    public String getNom(){
        return nom;
    }

    /**
     * Réécrit la méthode toString de l'objet Salle
     * @return
     */
    @Override
    public String toString() {
        return nom;
    }
}
