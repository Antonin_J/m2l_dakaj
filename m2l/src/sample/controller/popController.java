package sample.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import sample.Main;

import java.net.URL;
import java.util.ResourceBundle;

public class popController implements Initializable {

    @FXML
    Label infoLabel;

    private int lo;

    @FXML
    public void okBtn(){
        if (lo == 1){
            //recup l'instance ReservationController qui existe déjà
            ReservationController res = ConsulterController.loader.getController();
            res.clean();
        } else if (lo == 5) {
            Inscription in = ConsulterController.loader.getController();
            in.clean();
        }
        //ferme la fenêtre
        infoLabel.getScene().getWindow().hide();
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        lo = ReservationController.status;
        System.out.println("popup " + lo);
        if (lo == 0){
            infoLabel.setText("Problème lors de l'enregistrement");
        } else if (lo == 1){
            infoLabel.setText("Enregistrement effectué");
        } else if (lo == 2){
            infoLabel.setText("Veuillez choisir une heure");
        } else if (lo == 3){
            infoLabel.setText("Veuillez choisir une date");
        } else if (lo == 4) {
            infoLabel.setText("Problème lors de la connexion à la base de donnée");
        } else if (lo == 5){
            infoLabel.setText("Utilisateur ajouté");
        } else if (lo == 6){
            infoLabel.setText("Il faut remplire tous les champs");
        }
        else {
            infoLabel.setText("Problème inconnu");
        }
    }
}
